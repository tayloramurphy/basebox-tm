BaseBox
==========
#### Vagrant/Ansible Provisioner for ubuntu 64 12.04 lts

Preloads SciPy stack and configures both rethinkdb and the ipython notebook.
Can easily be configured to launch AWS instances.  


### Requirements:
* Vagrant >= 1.5.1
* Ansible >= 1.7.2
* VirtualBox >= 4.3.18



### Usage:
1. Clone the repo into a new project directory
2. `vagrant up`
3. On first `vagrant ssh` you will be prompted for a password to use with IPython.
4. run `ipython notebook` and you are all set with a preconfigured profile, rsynced project directories, authentication and SSL.
5. `vagrant destroy` will remove all traces of VM.
